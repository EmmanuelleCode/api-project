#API's whit some Lenguages and frameworks

**Requerimientos**
-Docker

**Conetenedores en:**
-Python
-Node
-Go
-React

**Instrucciones**

    #Elijir un nombre para la aplicacion:
    - cd /<project>
    - docker build -t <nombre_de_la_imagen> .
    #Ver las imagenes construidas
    - docker images
    #Seleccionar una imagen
    - docker run -it -p <local_port>:<expose_port> <nombre_de_la_imagen>

**Puertos de aplicaciones**
-django - 8000
-flask - 5000
-go - 3000
-node - 3000
-react -3000



